import React from 'react'
import { FeatureList } from '../compounds/featureList'
import { features } from '../../data/features'

export const FeatureListContainer = () => <FeatureList features={features} />;