import React from 'react'

import { silhouetteIcon } from '../../svg/icons'
import styles from './testimonial.styles.module.scss'

const wrapOrNoWrap = (condition, testimonial, wrapper, elem) =>  condition ? wrapper(testimonial, elem) : elem;

const wrapInLink = (testimonial, elem) => (
    <a className={styles.author} href={testimonial.refUrl} target="_blank">
        {elem}
    </a>
);

export const Testimonial = ({ testimonial }) => {
    const img = testimonial.imageUrl ? <img src={testimonial.imageUrl} alt={testimonial.author} /> : silhouetteIcon;

    const signature = <div>{testimonial.author} - <span className={styles.company}>{testimonial.company}</span></div>;

    const authorImg = wrapOrNoWrap(testimonial.refUrl, testimonial, wrapInLink, img);

    const authorName = wrapOrNoWrap(testimonial.refUrl, testimonial, wrapInLink, signature);

    return (
        <div className={styles.section}>
            <div className={styles.authorImage}>
                {authorImg}
            </div>
            <p className={styles.quote}>
                {testimonial.quote}
            </p>
            <div className={styles.author}>
                {authorName}
            </div>
        </div>
    )
};
