import React from 'react'
import Scroll from 'react-scroll'

import styles from './testimonial.styles.module.scss'

import { Testimonial } from './testimonial'

const Element = Scroll.Element;

export const TestimonialsList = ({ testimonials }) => {
    return (
        <div className={styles.list}>
            <Element name="testimonials" />
            {testimonials.map((testimonial, index) => <Testimonial testimonial={testimonial} key={index} />)}
        </div>
    )
};
