import React from 'react'

import styles from './sectionElement.styles.module.scss'
import { inlineStyles } from '../../../settings/styles'

export const SectionElementWithFloatImage = ({ title, description, icon, symbol, floatImage = 'left', iconColor = '#eaeaea'}) => {

    const inlineCss = floatImage === 'left' ? inlineStyles.section.floatLeft : inlineStyles.section.floatRight;
    const svgCss = Object.assign(inlineCss, { fill: iconColor });

    const content = typeof description === 'string' ? <p className={styles.description}>{description}</p>
                                                    : description.map((p, i) => <p className={styles.description} key={i}>{p}</p>);

    const featureSymbol = !!symbol && <span className={styles.symbol}>{symbol}</span>;

    const img = typeof icon === 'string' ? (
        <span className={styles.icon} style={inlineCss}>
            <img src={icon} />
        </span>
    ) : (
        <span className={styles.icon} style={svgCss}>
            {featureSymbol}
            {icon}
        </span>
    );

    return (
      <div className={styles.section} data-attr={`section-${floatImage}`}>
          {img}
        <div className="description">
            <h2 className={styles.header}>{title}</h2>
            {content}
        </div>
      </div>
    )
};

SectionElementWithFloatImage.propTypes = {
    floatImage: React.PropTypes.oneOf(['left', 'right']),
    title: React.PropTypes.string.isRequired,
    description: React.PropTypes.oneOfType([
        React.PropTypes.string,
        React.PropTypes.arrayOf(React.PropTypes.string),
    ]).isRequired,
    icon: React.PropTypes.oneOfType([
        React.PropTypes.string,
        React.PropTypes.element,
    ]),
};
