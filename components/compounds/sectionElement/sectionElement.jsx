import React from 'react'

import styles from './sectionElement.styles.module.scss'
import { inlineStyles } from '../../../settings/styles'

export const SectionElement = ({ title, description }) => {

    const content = typeof description === 'string' ? <p className={styles.description}>{description}</p>
        : description.map((p, i) => <p className={styles.description} key={i}>{p}</p>);

    return (
        <div className={styles.section}>
            <div className="description">
                <h2 className={styles.header}>{title}</h2>
                {content}
            </div>
        </div>
    )
};

SectionElement.propTypes = {
    title: React.PropTypes.string,
    description: React.PropTypes.oneOfType([
        React.PropTypes.string,
        React.PropTypes.arrayOf(React.PropTypes.string),
    ]).isRequired,
};
