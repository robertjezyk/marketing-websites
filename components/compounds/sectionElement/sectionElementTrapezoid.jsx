import React from 'react'

import styles from './sectionElementTrapezoid.styles.module.scss'
import { inlineStyles } from '../../../settings/styles'

export const SectionElementWithTrapezoid = ({ title, description, floatImage = 'left', imageUrl }) => {

    const content = typeof description === 'string' ? <p className={styles.p}>{description}</p>
                : description.map((p, i) => <p className={styles.p} key={i}>{p}</p>);

    const polygonType = floatImage === 'left' ? 'polygonLeft' : 'polygonRight';
    const headerAlign = floatImage === 'left' ? 'headerLeft' : 'headerRight';

    return (
        <div className={styles.section} data-attr={`section-${floatImage}`}>
            <div className={styles[polygonType]} style={ imageUrl ? { backgroundImage: `url(${imageUrl})` } : {} }></div>
            <div className={styles.description}>
                <h2 className={styles[headerAlign]}>{title}</h2>
                {content}
            </div>
        </div>
    );
};

SectionElementWithTrapezoid.propTypes = {
    floatImage: React.PropTypes.oneOf(['left', 'right']),
    title: React.PropTypes.string,
    description: React.PropTypes.oneOfType([
        React.PropTypes.string,
        React.PropTypes.arrayOf(React.PropTypes.string),
    ]).isRequired,
    imageUrl: React.PropTypes.string,
};