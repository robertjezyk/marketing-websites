import React from 'react'
import Scroll from 'react-scroll'

import { SubscribeLinkButton } from '../../molecules/buttons/subscribe'
import { TrialButton } from '../../molecules/buttons/trial'
import { ScrollDownArrow } from '../../molecules/buttons/scrollDownArrow'
import { landlordAccountzLogoIcon } from '../../svg/icons'

import styles from './hero.styles.module.scss'

const ScrollLink = Scroll.Link;

export const HeroContent = () => {
    return (
        <div className={styles.wrapper}>
            <section className={styles.content}>
                <h1 className={styles.h1}>{landlordAccountzLogoIcon}</h1>
                <p className={styles.p}>Desktop Accounting Software for Property Owners</p>
                <div className={styles.messageHolder}>
                    <h2 className={styles.h2}>Unlimited Tenancies</h2>
                    <h3 className={styles.h3}>£15.00 <span className={styles.span}> + VAT / month</span></h3>
                </div>
                <div className={styles.buttonsHolder}>
                    <SubscribeLinkButton />
                    <TrialButton info="This will start a download" />
                </div>
                <ScrollLink activeClass="active" to="testimonials" spy={true} smooth={true} duration={500}>
                    <ScrollDownArrow />
                </ScrollLink>
            </section>
        </div>
    );
};
