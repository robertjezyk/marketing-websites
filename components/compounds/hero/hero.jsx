import React, { PropTypes } from 'react'

import { HeroSection } from './heroSection'
import { HeroContent } from './heroContent'

export default class Hero extends React.Component {

    state = { height: this.props.height };

    componentDidMount() {
        this.setState({
            height: window.innerHeight + 'px'
        });
    }

    render() {
        return (
            <HeroSection height={this.state.height}>
                <HeroContent />
            </HeroSection>
        )
    }
}

Hero.propTypes = {
    height: PropTypes.string,
};
