import React from 'react'
import styles from './hero.styles.module.scss'

export const HeroSection = ({ children, height }) => {
    return (
        <div className={styles.hero} style={{ minHeight: height }}>
            {children}
        </div>
    );
};
