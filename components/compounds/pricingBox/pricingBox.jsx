import React from 'react'

import { SubscribeButton } from '../../../components/molecules/buttons/subscribe'

import styles from './pricingBox.styles.module.scss'

export const PricingBox = ({ companies, subscribe, handlePlusClick, handleMinusClick }) => {
    return (
        <div className={styles.subscribeWrapper}>
            <h1 className={styles.h1}>Subscribe</h1>
            <div className={styles.pricingBox}>
                <h3 className={styles.h3}>Pick number of companies</h3>
                <p className={styles.p}>(max 5)</p>
                <div className={styles.addBox}>
                    <div className={styles.buttons}>
                        <button className={styles.button} onClick={handlePlusClick}>+</button>
                        <button className={styles.button} onClick={handleMinusClick}>-</button>
                    </div>
                    <span className={styles.number}>{companies}</span>
                </div>

                <p className={styles.price}>
                    {subscribe[companies].price}
                    <span className={styles.appendix}>per month</span>
                </p>

                <SubscribeButton id={subscribe[companies].id} />
            </div>
        </div>
    )
};
