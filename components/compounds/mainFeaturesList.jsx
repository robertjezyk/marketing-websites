import React from 'react'

import { MediaElement } from './mediaElement/mediaElement'

import { tenants, icon, profitLoss, inputTransactions, alert, vat, advice } from '../../components/svg/icons'
import styles from '../../css/flexgrid.module.css'

export const MainFeaturesList = ({ data }) => {
    const featureIcons = { tenants, icon, profitLoss, inputTransactions, alert, vat, advice };
    const noOfFeatures = 6;

    return (
        <div className={styles.row}>
            {data.map((o, i) => {
                return i < noOfFeatures && <MediaElement key={i} icon={featureIcons[o.icon]} title={o.header} description={o.text} />
            })}
        </div>
    )
};

MainFeaturesList.propTypes = {
    data: React.PropTypes.array.isRequired
};
