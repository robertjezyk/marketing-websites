import React from 'react'
import { Link } from 'react-router'
import { prefixLink } from 'gatsby-helpers'
import { logoIcon } from '../../svg/icons'
import styles from './menu.styles.module.scss'

import { TrialButton } from '../../molecules/buttons/trial'

export const Menu = () => {
    return (
        <div className={styles.menu}>
            <Link to={prefixLink('/')} className={styles.logoLink}>
                {logoIcon}
            </Link>
            <nav className={styles.nav}>
                <TrialButton type="small" />
                <Link to={prefixLink('/subscribe/')} className={styles.link}>Subscribe</Link>
                <Link to={prefixLink('/features/')} className={styles.link}>Features</Link>
            </nav>
        </div>
    );
};
