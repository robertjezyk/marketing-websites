import React from 'react'
import Trianglify from 'react-trianglify'
import { SectionElementWithFloatImage } from './sectionElement/sectionElementWithFloatImage'
import * as abstractGraphics from '../svg/abstracts'
import { getRandomSVGIcon } from '../../utils/helpers'

export const FeatureList = ({ features }) => {
    return (
        <div>
            {features.map((feature, i) => {
                //const icon = feature.image ? `../images/features/${feature.image}` : getRandomSVGIcon(abstractGraphics);
                //const symbol = feature.image === '' ? feature.name.charAt(0) : null; //TODO: Create sensible algorithm for creating shortcuts

                const icon = feature.image ? `../images/features/${feature.image}` : <Trianglify output="svg" />;
                const symbol = feature.image === '' ? feature.symbol : null;

                return <SectionElementWithFloatImage title={feature.name}
                                                     description={feature.description}
                                                     icon={icon}
                                                     floatImage={i % 2 === 0 ? 'left' : 'right'}
                                                     iconColor="rgb(234, 234, 234)"
                                                     symbol={symbol}
                                                     key={i} />
                }
            )}
        </div>
    );
};