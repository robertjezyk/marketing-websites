import React from 'react'

import styles from './mediaElement.styles.module.scss'

export const MediaElement = ({ icon, title, description }) => {
      return (
          <div className={styles.media}>
              <div className={styles.header}>
                  <span className={styles.icon}>{icon}</span>
                  <h2 className={styles.h2}>{title}</h2>
              </div>
              <p className={styles.description}>{description}</p>
          </div>
      )
};
