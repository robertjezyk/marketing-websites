import React from 'react'

import { Wrapper } from '../../molecules/wrapper'
import { Section } from '../../molecules/section'

import { facebookIcon, twitterIcon, envelopeIcon, logoIcon } from '../../svg/icons'
import styles from './footer.styles.module.scss'

export const Footer = () => {
    const date = new Date().getFullYear();
    return (
        <div className={styles.section}>
            <div className={styles.wrapper}>
                <footer className={styles.footer}>

                    <section className={styles.linksSection}>
                        <ul className={styles.linksList}>
                            <li className={styles.li}>
                                <a href="https://www.accountz.com/pages/shop-terms-conditions" className={styles.link} target="_blank">T&C</a>
                            </li>
                            <li className={styles.li}>
                                <a href="http://businesshelp.accountz.com/" className={styles.link} target="_blank">Help</a>
                            </li>
                        </ul>
                    </section>

                    <section className={styles.contactSection}>
                        <div className="vcard">
                            <div className="adr">
                                <h4 className="org">Accountz Ltd</h4>
                                <span className="street-address">South Fens Business Centre</span><br />
                                <span className="street-address">Fenton Way</span><br />
                                <span className="locality">Chatteris, </span>
                                <span className="region">Cambridgeshire, </span>
                                <span className="postal-code">PE16 6TT</span><br />
                                <p className="tel"><b>+44 (0) 13 5469 1650</b></p>
                            </div>
                            <div className={styles.email}>
                                <span className={styles.emailIcon}>{envelopeIcon}</span>
                                <span className={styles.emailAddress}>support@accountz.com</span>
                            </div>
                        </div>
                        <div className={styles.social}>
                            <a className={styles.icon} href="https://www.facebook.com/Accountz/">{facebookIcon}</a>
                            <a className={styles.icon} href="https://twitter.com/accountz/">{twitterIcon}</a>
                        </div>
                    </section>

                    <section className={styles.brandSection}>
                        <div className={styles.copyRights}>© Copyright Accountz.com {date}</div>
                        <div className={styles.brandLogo}>
                            {logoIcon}
                        </div>
                    </section>
                </footer>
            </div>
        </div>
    )
};
