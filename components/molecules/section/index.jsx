import React from 'react'

import { Wrapper } from '../wrapper'
import { ScrollToTop } from '../buttons/scrollToTop'

export const Section = ({ children, className, scrollToTop, styles = {}, background = "" }) => {

    const css = Object.assign({ background }, styles);

    return (
        <div className={className} style={css}>
            <Wrapper>
                {children}
                {!!scrollToTop && <ScrollToTop />}
            </Wrapper>
        </div>
    );
};

Section.propTypes = {
    className: React.PropTypes.string,
    scrollToTop: React.PropTypes.bool,
    styles: React.PropTypes.object,
    background: React.PropTypes.string,
};
