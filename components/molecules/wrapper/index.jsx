import React from 'react'
import styles from './index.styles.module.scss'

export const Wrapper = ({ children }) => {
    return (
        <div className={styles.wrapper}>
            {children}
        </div>
    );
};
