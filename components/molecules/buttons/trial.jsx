import React from 'react'
import ReactGA from 'react-ga';
import platform from 'platform'

import { findDownloader } from '../../../utils/helpers'
import styles from './buttons.styles.module.scss'

export const TrialButton = ({ type = '', info }) => {
    const css = type === 'small' ? styles.trialSmall : styles.trial;

    const infoTag = !!info && <span className={styles.info}>{info}</span>;

    const os = platform.os;
    const downloader = findDownloader(os.architecture, os.family);

    const logGAEvent = () => {
        console.log(`Trial Button Clicked - ${os.family}${os.architecture}`);
        ReactGA.event({
            category: 'Trial Button',
            action: 'click',
            label: `Platform - ${os.family}${os.architecture}`,
            value: 1
        });
    };

    return (
        <div className={styles.trialContainer}>
            <a href={downloader}
               className={css}
               onClick={logGAEvent}>Download Free Trial</a>
            {infoTag}
        </div>
    );
};
