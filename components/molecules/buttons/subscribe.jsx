import React from 'react'
import { Link } from 'react-router'
import { prefixLink } from 'gatsby-helpers'
import styles from './buttons.styles.module.scss'

export const SubscribeLinkButton = () => {
    return (
            <Link to={prefixLink('/subscribe/')} className={styles.subscribeLink}>Subscribe for £15</Link>
    );
};

export const SubscribeButton = ({ id }) => {
    return (
            <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                <input name="cmd" value="_s-xclick" type="hidden" />
                <input name="hosted_button_id" value={id} type="hidden" />
                <button name="submit" type="submit" className={styles.subscribeButton}>Subscribe</button>
            </form>
    );
};

SubscribeButton.propTypes = {
    id: React.PropTypes.string.isRequired,
};
