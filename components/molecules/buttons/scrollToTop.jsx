import React from 'react'
import Scroll from 'react-scroll'

import styles from './buttons.styles.module.scss'

const scroll = Scroll.animateScroll;

export const ScrollToTop = () => {
    return (
        <a onClick={scroll.scrollToTop}
           className={styles.scrollToTop}>
            Top
        </a>
    );
};
