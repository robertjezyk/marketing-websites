import React from 'react'

import { arrowDownIcon } from '../../svg/icons'
import styles from './buttons.styles.module.scss'

export const ScrollDownArrow = () => {
    return (
        <div className={styles.arrowDown}>{arrowDownIcon}</div>
    );
};