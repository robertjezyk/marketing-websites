export const lorem = {
    index: {
        count: 2,                      // Number of words, sentences, or paragraphs to generate. 
        units: 'paragraphs',            // Generate words, sentences, or paragraphs. 
        sentenceLowerBound: 5,         // Minimum words per sentence. 
        sentenceUpperBound: 15,        // Maximum words per sentence. 
        paragraphLowerBound: 3,        // Minimum sentences per paragraph. 
        paragraphUpperBound: 7,        // Maximum sentences per paragraph. 
        format: 'plain',               // Plain text or html
        random: Math.random,           // A PRNG function. Uses Math.random by default
    },
    about: {
        long: {
                count: 10,                     // Number of words, sentences, or paragraphs to generate. 
                units: 'sentences',            // Generate words, sentences, or paragraphs. 
                sentenceLowerBound: 5,         // Minimum words per sentence. 
                sentenceUpperBound: 15,        // Maximum words per sentence. 
                paragraphLowerBound: 3,        // Minimum sentences per paragraph. 
                paragraphUpperBound: 7,        // Maximum sentences per paragraph. 
                format: 'plain',               // Plain text or html
                random: Math.random,           // A PRNG function. Uses Math.random by default
            },
        medium: {
            count: 3,                      // Number of words, sentences, or paragraphs to generate.
            units: 'sentences',                 // Generate words, sentences, or paragraphs.
            format: 'plain',               // Plain text or html
            random: Math.random,           // A PRNG function. Uses Math.random by default
        },
        short: {
            count: 4,                      // Number of words, sentences, or paragraphs to generate. 
            units: 'words',                 // Generate words, sentences, or paragraphs.
            format: 'plain',               // Plain text or html
            random: Math.random,           // A PRNG function. Uses Math.random by default
        },
    }
} 