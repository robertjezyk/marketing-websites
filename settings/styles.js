export const inlineStyles = {
  headroom: {
    blue: {
      background: '#3498db',
      backgroundImage: 'linear-gradient(rgba(55, 157, 226, 0.9), rgb(47, 137, 197))',
      borderBottom: '1px solid rgb(40, 113, 162)',
      boxShadow: 'rgba(0, 0, 0, 0.2) 0px 3px 10px',
    }
  },
  section: {
    floatLeft: {
      float: 'left',
      margin: "30px 40px 0 0",
      shapeOutside: "circle(170px at 150px 180px)",
      border: '10px solid rgb(234, 234, 234)',
    },
    floatRight: {
      float: 'right',
      margin: "30px 0 0 40px",
      shapeOutside: "circle(170px at 190px 180px)",
      border: '10px solid rgb(234, 234, 234)',
    }
  }
};
