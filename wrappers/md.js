import React from 'react'
import 'css/markdown-styles.css'
import Helmet from "react-helmet"
import { config } from 'config'
import { Wrapper } from '../components/molecules/wrapper'

module.exports = React.createClass({
    propTypes () {
        return {
            router: React.PropTypes.object,
        }
    },
    render () {
        const post = this.props.route.page.data;
        const type = post.path !== "/404.html" ? "article" : "website";
        const url = config.hostName + this.props.location.pathname;
        const imageUrl = post.imageUrl ? config.hostName + post.imageUrl : config.hostName + '/favicons/manifest/ms-icon-310x310.png';

        return (
            <div className="markdown">
                <Helmet title={`${config.siteTitle} | ${post.title}`}
                        meta={[
                            { "name": "description", "content": post.description },
                            { "property": "og:title", "content": post.title },
                            { "property": "og:url", "content": url },
                            { "property": "og:type", "content": type },
                            { "property": "og:description", "content": post.description },
                            { "property": "og:image", "content": imageUrl },
                        ]}
                        link={[
                            {"rel": "canonical", "href": url},
                        ]} />

                <Wrapper>
                    <h1>{post.title}</h1>
                    <div dangerouslySetInnerHTML={{ __html: post.body }} />
                </Wrapper>
            </div>
        )
    },
});
