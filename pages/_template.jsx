import React from 'react'
import Headroom from 'react-headroom'

import { Wrapper } from '../components/molecules/wrapper/index'
import { Footer } from '../components/compounds/footer/footer'
import { Menu } from '../components/compounds/menu/menu'

import { inlineStyles } from '../settings/styles'
import '../css/markdown-styles'
import '../css/global-styles'

module.exports = React.createClass({
  propTypes () {
    return {
      children: React.PropTypes.any,
    }
  },

  render () {
    let headroom;

    switch (this.props.location.pathname) {
      case "/":
          break;

      default:
          headroom = inlineStyles.headroom.blue;
          break;
    }

    return (
      <div>
        <Headroom style={headroom}>
            <Menu />
        </Headroom>

        {this.props.children}

        <Footer />
      </div>
    )
  }
});
