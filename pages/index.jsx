import React from 'react'
import Helmet from "react-helmet"
import Scroll from 'react-scroll'

import { prefixLink } from 'gatsby-helpers'
import { config } from 'config'

import { createArrayFromObjectKeys } from '../utils/helpers'

import { mainFeatures } from '../data/mainFeatures'
import { testimonials } from '../data/testimonials'
import { reports, about, privacy } from '../data/indexPageContent'

//import { SectionElement } from '../components/compounds/sectionElement/sectionElement'
import { SectionElementWithTrapezoid } from '../components/compounds/sectionElement/sectionElementTrapezoid'
import { Wrapper } from '../components/molecules/wrapper'
import { Section } from '../components/molecules/section'
import { TestimonialsList} from '../components/compounds/testimonial/testimonialList'
import { MainFeaturesList } from '../components/compounds/mainFeaturesList'
import Hero from '../components/compounds/hero/hero'

const Events = Scroll.Events;
const scrollSpy = Scroll.scrollSpy;

export default class Index extends React.Component {
    componentDidMount () {
        Events.scrollEvent.register('begin', (to, element) => {
            console.log("begin", arguments);
        });
        Events.scrollEvent.register('end', (to, element) => {
            console.log("end", arguments);
        });
        scrollSpy.update();
    }

    componentWillUnmount () {
        Events.scrollEvent.remove('begin');
        Events.scrollEvent.remove('end');
    }

    render () {
        const url = config.hostName + this.props.location.pathname;
        const imageUrl = config.hostName + '/favicons/manifest/ms-icon-310x310.png';

        return (
          <div>
            <Helmet title={config.siteTitle}
                    meta={[
                      { "name": "description", "content": "Landlord Accounting Software. Designed to make property management quick, simple and affordable. Take a free trial and we will help set up your books for free." },
                      { "name": "keywords", "content": "accounting software, landlord accounting" },
                      { "property": "og:title", "content": config.siteTitle },
                      { "property": "og:url", "content": url },
                      { "property": "og:type", "content": "website" },
                      { "property": "og:description", "content": "Landlord Accounting Software. Designed to make property management quick, simple and affordable. Take a free trial and we will help set up your books for free." },
                      { "property": "og:image", "content": imageUrl },
                    ]}
                    link={[
                        {"rel": "canonical", "href": url},
                    ]} />

            <Hero />

            <Section background="whitesmoke"
                     styles={{ borderBottom: '1px solid #d2d1d0' }}>
              <TestimonialsList testimonials={testimonials} />
            </Section>

            <Section scrollToTop>
              <MainFeaturesList data={createArrayFromObjectKeys(mainFeatures)} />
            </Section>

            <Section background="#d2d1d0"
                     styles={{ borderTop: '1px solid #bbbbbb' }}
                     scrollToTop>
                <SectionElementWithTrapezoid title="About Us"
                                             description={about}
                                             imageUrl="./images/about-us.jpg" />
            </Section>
            <Section styles={{ borderTop: '1px solid rgb(189, 189, 189)' }}
                     scrollToTop>
                <SectionElementWithTrapezoid title="Reports"
                                             floatImage="right"
                                             imageUrl="./images/reports.jpg"
                                             description={reports} />
            </Section>
            <Section background="#d2d1d0"
                     styles={{ borderTop: '1px solid #bbbbbb' }}
                     scrollToTop>
                <SectionElementWithTrapezoid imageUrl="./images/privacy.jpg"
                                             title="Privacy and Security"
                                             description={privacy} />
            </Section>
          </div>
        )
    }
}
