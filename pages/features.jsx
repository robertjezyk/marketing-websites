import React from 'react'
import Helmet from "react-helmet"
import { config } from 'config'

import { H1 } from '../components/molecules/headers'
import { Section } from '../components/molecules/section'
import { FeatureListContainer } from '../components/containers/FeatureListContainer'

export default class Features extends React.Component {
    render () {
        const url = config.hostName + this.props.location.pathname;
        const imageUrl = config.hostName + '/favicons/manifest/ms-icon-310x310.png';

        return (
          <div>
              <Helmet title={`${config.siteTitle} | Features`}
                      meta={[
                          { "name": "description", "content": "Landlord Accounting Software. Designed to make property management quick, simple and affordable. Take a free trial and we will help set up your books for free." },
                          { "name": "keywords", "content": "accounting software, landlord accounting" },
                          { "property": "og:title", "content": config.siteTitle },
                          { "property": "og:url", "content": url },
                          { "property": "og:type", "content": "website" },
                          { "property": "og:description", "content": "Landlord Accounting Software. Designed to make property management quick, simple and affordable. Take a free trial and we will help set up your books for free." },
                          { "property": "og:image", "content": imageUrl },
                        ]}
                      link={[
                            {"rel": "canonical", "href": url},
                        ]} />

            <Section background="#dcdcdc"
                     scrollToTop>
                <H1>Features</H1>
                <FeatureListContainer />
            </Section>
          </div>
        );
    }
}
