import React from 'react'
import Helmet from 'react-helmet'
import { Link } from 'react-router'
import { prefixLink } from 'gatsby-helpers'
import { config } from 'config'

import loremIpsum from 'lorem-ipsum'
import { lorem } from '../settings/lorem'
import { subscribe } from '../data/paypal'

import { Section } from '../components/molecules/section'
import { SectionElementWithFloatImage } from '../components/compounds/sectionElement/sectionElementWithFloatImage'
import { PricingBox } from '../components/compounds/pricingBox/pricingBox'

import { silhouetteIcon } from '../components/svg/icons'

export default class Subscribe extends React.Component {

    state = { count: 1 };

    handlePlusClick = () => {
        if (this.state.count < 5) {
            this.setState({ count: this.state.count + 1 });
        }
    };

    handleMinusClick = () => {
        if (this.state.count > 1) {
            this.setState({ count: this.state.count - 1 });
        }
    };

    render () {
        const url = config.hostName + this.props.location.pathname;
        const imageUrl = config.hostName + '/favicons/manifest/ms-icon-310x310.png';

        return (
            <div>
                <Helmet title={`${config.siteTitle} | Subscribe`}
                        meta={[
                          { "name": "description", "content": "Landlord Accounting Software. Designed to make property management quick, simple and affordable. Take a free trial and we will help set up your books for free." },
                          { "name": "keywords", "content": "accounting software, landlord accounting" },
                          { "property": "og:title", "content": config.siteTitle },
                          { "property": "og:url", "content": url },
                          { "property": "og:type", "content": "website" },
                          { "property": "og:description", "content": "Landlord Accounting Software. Designed to make property management quick, simple and affordable. Take a free trial and we will help set up your books for free." },
                          { "property": "og:image", "content": imageUrl },
                        ]}
                        link={[
                            {"rel": "canonical", "href": url},
                        ]} />

                <Section background="#50adea" styles={{ borderBottom: '3px solid', borderBottomColor: 'rgba(0, 0, 0, 0.3)'}}>
                    <PricingBox subscribe={subscribe}
                                handlePlusClick={this.handlePlusClick}
                                handleMinusClick={this.handleMinusClick}
                                companies={this.state.count} />
                </Section>

                <Section scrollToTop>
                    <SectionElementWithFloatImage icon={silhouetteIcon}
                                                  floatImage="left"
                                                  title={loremIpsum(lorem.about.short)}
                                                  description={loremIpsum(lorem.about.long)} />
                    <SectionElementWithFloatImage icon={silhouetteIcon}
                                                  floatImage="right"
                                                  title={loremIpsum(lorem.about.short)}
                                                  description={loremIpsum(lorem.about.long)} />
                </Section>
            </div>
        )
    }
}
