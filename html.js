import React from 'react'
import Helmet from "react-helmet"

import { prefixLink } from 'gatsby-helpers'
import { TypographyStyle, GoogleFont } from 'react-typography'
import typography from './utils/typography'
import { config } from 'config'

const BUILD_TIME = new Date().getTime();

module.exports = React.createClass({
  propTypes () {
    return {
      body: React.PropTypes.string,
    }
  },
  render () {
    const head = Helmet.rewind();

    let css;
    if (process.env.NODE_ENV === 'production') {
      css = <style dangerouslySetInnerHTML={{ __html: require('!raw!./public/styles.css') }} />
    }

    const script = (process.env.NODE_ENV === 'production' && config.noProductionJavascript) ? null : (<script src={prefixLink(`/bundle.js?t=${BUILD_TIME}`)} />);

    return (
      <html lang="en">
        <head>
          <link rel="apple-touch-icon" sizes="57x57" href="../favicons/manifest/apple-icon-57x57.png" />
          <link rel="apple-touch-icon" sizes="60x60" href="../favicons/manifest/apple-icon-60x60.png" />
          <link rel="apple-touch-icon" sizes="72x72" href="../favicons/manifest/apple-icon-72x72.png" />
          <link rel="apple-touch-icon" sizes="76x76" href="../favicons/manifest/apple-icon-76x76.png" />
          <link rel="apple-touch-icon" sizes="114x114" href="../favicons/manifest/apple-icon-114x114.png" />
          <link rel="apple-touch-icon" sizes="120x120" href="../favicons/manifest/apple-icon-120x120.png" />
          <link rel="apple-touch-icon" sizes="144x144" href="../favicons/manifest/apple-icon-144x144.png" />
          <link rel="apple-touch-icon" sizes="152x152" href="../favicons/manifest/apple-icon-152x152.png" />
          <link rel="apple-touch-icon" sizes="180x180" href="../favicons/manifest/apple-icon-180x180.png" />
          <link rel="icon" type="image/png" sizes="192x192" href="../favicons/manifest/android-icon-192x192.png" />
          <link rel="icon" type="image/png" sizes="32x32" href="../favicons/manifest/favicon-32x32.png" />
          <link rel="icon" type="image/png" sizes="96x96" href="../favicons/manifest/favicon-96x96.png" />
          <link rel="icon" type="image/png" sizes="16x16" href="../favicons/manifest/favicon-16x16.png" />

          <meta name="msapplication-TileColor" content="#ffffff" />
          <meta name="msapplication-TileImage" content="../favicons/manifest/ms-icon-144x144.png" />
          <meta name="theme-color" content="#ffffff" />

          <meta charSet="utf-8" />
          <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
          <meta name="viewport" content="width=device-width, initial-scale=1.0" />

          {head.title.toComponent()}
          {head.meta.toComponent()}
          <TypographyStyle typography={typography} />
          <GoogleFont typography={typography} />
          {css}

          <script dangerouslySetInnerHTML={{__html: `
            (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:327524,hjsv:5};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
          })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');`
         }}></script>

        </head>

        <body>
          <script type="application/ld+json" dangerouslySetInnerHTML={{__html: `
            {
              "@context": "http://schema.org",
              "@type": "Organization",
              "name": "Accountz",
              "url": "http://www.landlordaccountz.com",
              "sameAs": [
              "https://www.facebook.com/Accountz/",
              "https://plus.google.com/+AccountzLtd",
              "https://twitter.com/accountz/"
              ]
            }`
          }}></script>
          <a href="https://plus.google.com/110833302773263755599" rel="publisher" />

          <div id="react-mount" dangerouslySetInnerHTML={{ __html: this.props.body }} />

          {script}

          <script dangerouslySetInnerHTML={{__html: `
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
            ga('create', 'UA-86849163-1', 'auto');
            ga('send', 'pageview');`
          }}></script>
        </body>
      </html>
    )
  },
});
