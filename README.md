# Requirements
- Python v2
- Nodejs + npm

# Install
- `npm install -g gatsby` 

# Run project
- `gatsby develop` in Git bash | Command prompt
- development server is accessible at localhost:8000 

# Resources
- social media logo pack - http://www.flaticon.com/packs/social-media-logo-collection
