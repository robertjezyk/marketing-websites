import { installers } from '../data/installers'

export const findDownloader = (architecture, family) => {
    switch (family) {
        case 'Windows':
            return architecture === 64 ? installers.win64 : installers.win32;
            break;
        case 'Linux':
            return installers.linux;
            break;
        case 'MacOS':
            return installers.mac;
            break;
        default:
            return '#';
            break;
    }
};

export const createArrayFromObjectKeys = (featuresObject) => {
    return Object.keys(featuresObject).map(key => ({
        id: key.split(" ").join("-").toLowerCase(),
        header: key,
        text: featuresObject[key].text,
        icon: featuresObject[key].icon,
    }));
};

export const getRandomIntExclusive = (minimum, maximum) => {
    const min = Math.ceil(minimum);
    const max = Math.floor(maximum);
    return Math.floor(Math.random() * (max - min)) + min;
};

export const getRandomSVGIcon = (icons) => {
    const keys = Object.keys(icons);
    const index = getRandomIntExclusive(0, keys.length);

    return icons[keys[index]];
};
