export const installers = {
    win64: "http://download.accountz.co.uk/business/install_business_accountz_v3_win_64bit.exe",
    win32: "http://download.accountz.co.uk/business/install_business_accountz_v3_win.exe",
    mac: "window.location='http://download.accountz.co.uk/business/install_business_accountz_v3.dmg",
    linux: "http://download.accountz.co.uk/business/install_business_accountz_v3_linux.sh",
};
