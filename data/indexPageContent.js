export const about = [
    "Landlord Accountz is a product from Accountz Ltd . We provide a range of accounting software solutions and have been creating accounting software for over 20 years!",
    "Based in the UK we are a proud British company which provides outstanding support to our users, when you call us you get straight through to a real person. No machines, no robots and no headaches.",
    "The company was founded by chartered accountant John Talbot (FCA) who has dealt with some of the largest businesses in the UK, giving him the knowledge required to create a programme that is simple yet powerful to help a business of any size.",
];

export const reports = [
    "The advanced reporting in Landlord Accountz puts you at the forefront of your finances.It’s not just landlord accounting software it’s a full property management system.",
    "Closely monitor your expenses and income, keep an eye on who owes you money, how much you are due to pay the VAT man and most importantly run live profit and loss reports, nominal ledger reports and create a trial balance and trial balance sheet.",
    "We give you a simple grasp over your finances whilst providing the power you need to run some of the most complicated reports.",
];

export const privacy = [
   "Our aim is to ensure our clients’ data is always accessible and most importantly confidential and secure. Which is why Landlord Accountz is desktop based and will NEVER share your data with any third party.",
   "Our landlord accounting software is a desktop based app which is not dependent on the internet. We do offer optional mainFeatures at no extra cost that utilise the internet, such as Online Backup and Recovery and multiple access locations. However these files are secured by a 256 BIT SSL encryption and stored on Amazon servers in Ireland.",
   "Due to the nature of Accountz you can even work offline and then upload your data into a secure Online Backup at a convenient time. Of course these mainFeatures are completely optional!",
   //"We will always put your companies’ privacy first.",
];
