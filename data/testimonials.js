export const testimonials = [
    //{
    //    author: "John Malkovitch",
    //    quote: "Some directors expect you to do everything; write, be producer, psychiatrist. Some just want you to die in a tragic accident during the shooting so they can get the insurance",
    //    company: "Alphabet Inc.",
    //    imageUrl: "https://scontent-lhr3-1.xx.fbcdn.net/v/t1.0-1/p160x160/14650676_1744889872426289_2452902574714276311_n.jpg?oh=52d16caa2b6ef986ffef15759d2eb7be&oe=589BE4E2",
    //    refUrl: "https://www.facebook.com/JohnMalkovich",
    //},
    //{
    //    author: "Jack Nicholson",
    //    quote: "There's only two people in your life you should lie to... the police and your girlfriend",
    //    company: "Bonanza Ltd",
    //    imageUrl: "https://scontent-lhr3-1.xx.fbcdn.net/v/t1.0-1/c17.17.207.207/s160x160/532059_102861449883485_1459824632_n.jpg?oh=4c11de34f1a12e8bc4f0cf8afc13a0de&oe=58C8CC0B",
    //    refUrl: "https://www.facebook.com/jack.nicholson.14289",
    //},
    //{
    //    author: "Jack Kerouac",
    //    quote: "All human beings are also dream beings. Dreaming ties all mankind together",
    //    company: "Horse & Sons",
    //    imageUrl: "https://scontent-lhr3-1.xx.fbcdn.net/v/t1.0-1/c6.0.160.160/p160x160/1176206_1438690823040628_5852406860075267823_n.jpg?oh=a63f28a91250e8fd8ae6bb0a19273668&oe=58C3FCA5",
    //    refUrl: "https://www.facebook.com/Jack-Kerouac-1438690426374001",
    //},
    {
        author: "Neil Stockdale",
        quote: "I made the plunge to buy a Mac and subscribed to Accountz. What comes across is the fantastic customer service and usability which is simple to use across 3 business’s. It lets me see how profitable each property is and I have found that there is simply nothing Accountz can’t do.",
        company: "Private Landlord",
        imageUrl: "",
        refUrl: "",
    },
    {
        author: "David Nicholls",
        quote: "I was impressed with how intuitive the system appeared but what makes Accountz stand out is the telephone support offered. It is some of the best customer support I have ever experienced. I now spend 30min a month on my accounts, they are accurate to the penny and I have reduced my accountancy costs.",
        company: "TFL Northern Investment Portfolio",
        imageUrl: "",
        refUrl: "",
    },
];