export const mainFeatures = {
    "Unlimited tenancies": {
        "text": "We don’t feel the need to restrict a landlord’s growth potential. Which is why our landlord accounting software allows landlords to monitor an unlimited amount of tenants with no price increases.",
        "icon": "tenants",
    },
    "Alerts you to late rental payments": {
        "text": "Nothing is worse than keeping track of who owes you money, our landlord accounting software will keep track of and alert you to any late payments. Giving you more time to relax and enjoy the nicer things in life.",
        "icon": "alert",
    },
    "Profit and loss for unlimited different properties": {
        "text": "Our property management system lets landlords monitor each of their properties finances individually within our accounting software. Making it easy to spot properties which are not turning a profit, so you can make the right decisions to make your portfolio more profitable.",
        "icon": "profitLoss",
    },
    "Automatically inputs transactions": {
        "text": "Don’t bother inputting the same transactions month after month. Let your Landlord Accounting Software do the hard work for you, any recurring transactions can be entered automatically by Landlord Accountz.",
        "icon": "inputTransactions",
    },
    "Submits vat returns and gives you info for self-assessment": {
        "text": "Submit your VAT returns directly through your landlord accounting software to HMRC, it’s an extremely easy process and will save you more than what Landlord Accountz will cost you! It will even provide you with the figures you need to submit your self-assessment tax return.",
        "icon": "vat",
    },
    "Bookkeeping advice": {
        "text": "Get advice on how to manage your books from fully qualified bookkeepers. We have been managing peoples books for over 20 years and are always happy to pass on what we have learned to landlords to make running your books as simple as possible.",
        "icon": "advice",
    }
};
