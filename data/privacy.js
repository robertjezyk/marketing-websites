export const privacy = [
    "Our aim is to ensure our clients’ data is always accessible and most importantly confidential and secure. Which is why Landlord Accountz will NEVER share your data with any third party.",
    "Our landlord accounting software is a desktop based app which is not dependent on the internet. We offer optional mainFeatures at no extra cost that utilise the internet such as Online Backup and Recovery and multiple access locations, but these files are secured by a 256 BIT SSL encryption and stored on Amazon servers in Ireland. You don’t have to use this functionality if you don’t want to!",
    "The servers are monitored 24 hours a day and not affected by American data laws which can allow access to data upon federal request.",
    "We will always put your companies’ privacy first."
];
